**Introduction**
An undergraduate final year project at the School of Computing, University of Leeds.
The project was developed with Ubuntu 14.04 (LTS), ROS Indigo and Python2. 
The robot used was Turtlebot 2.

2.2 Make use of project's wiki (Instructions how to run the project included)
Detailed instructions on how to install and run the project is available in the Wiki of the project.
Wiki can be found [here](https://gitlab.com/sc17zm/dissertation/-/wikis/home)