''''''''''''''''''''''''''''''''''''''''''''''''''''
Processing of the image
'''''''''''''''''''''''''''''''''''''''''''''''''''''

# ROS and OpenCV packages
from __future__ import division
import os
import cv2

from cv_bridge import CvBridge, CvBridgeError

def img_processing(img):
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    RGB raw image to bgr Mat format
    Args:image_raw: RGB raw image
    Return:OpenCV processed image
    '''''''''''''''''''''''''''''''''''''''''''''''''''''
    # Blur the image
    img_blur = cv2.medianBlur(img, 3)

    # Convert the image into grayscale
    img_greyscale = cv2.cvtColor(img_blur, cv2.COLOR_BGR2GRAY)

    # Adaptive threshold
    img_thresholded = cv2.adaptiveThreshold(img_greyscale,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,17,22)

    # Canny edge detector
    img_canny = cv2.Canny(img, 100, 200)

    # Return the processed image
    return img_canny

# Get the biggest contour
def get_contour(img):
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Find the contours according to the processed image
    Args: processed image in OpenCV format
    Return: list of the max index and the contours
    '''''''''''''''''''''''''''''''''''''''''''''''''''''
    # Find the contours
    (contours, _) = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Compute areas for contours found
    cnt_areas = [cv2.contourArea(contours[n]) for n in range(len(contours))]

    # Get the biggest index
    maxCnt_index = cnt_areas.index(max(cnt_areas))

    # return the contours and the max index
    return contours, maxCnt_index


def toMAT(raw_image):
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    RGB raw image to bgr Mat format
    Args:image_raw: RGB raw image
    Return:OpenCV image
    '''''''''''''''''''''''''''''''''''''''''''''''''''''
    try:
        # RGB raw image to bgr Mat format
        cv_image = CvBridge().imgmsg_to_cv2(raw_image, 'bgr8')

        # Returns the image in openCV format
        return cv_image

    except Exception as CvBridgeError:
        print('Error while converting the image: ', CvBridgeError)
