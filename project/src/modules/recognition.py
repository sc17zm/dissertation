''''''''''''''''''''''''''''''''''''''''''''''''''''
    AR Marker Recognition
    The class makes uses a flag to return if the markers
    has been recognised or not
'''''''''''''''''''''''''''''''''''''''''''''''''''''
from __future__ import division
import rospy

class Recognition:

    def __init__(self):
        ''''''''''''''''''''''''''''''''''''''''''''''''''''
        Class constructor
        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        # Flag
        self.recognised = False



    def is_recognised(self):
        ''''''''''''''''''''''''''''''''''''''''''''''''''''
        Getter method that is used by the main to check
        if the image has been recognised
        Return: boolean: True or False
        '''''''''''''''''''''''''''''''''''''''''''''''''''''

        return self.recognised

    def reset_rec_flag(self):
        ''''''''''''''''''''''''''''''''''''''''''''''''''''
        Resets recognised flag
        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        self.recognised = False
