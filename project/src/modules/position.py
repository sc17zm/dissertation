from __future__ import division
''''''''''''''''''''''''''''''''''''''''''''''''''''
    AR Marker Position.
    The class centering detects the AR marker
    and centers the image using contours.
'''''''''''''''''''''''''''''''''''''''''''''''''''''

'''ROS and OpenCV packages'''
import cv2
import rospy
import tf

# Messages
from geometry_msgs.msg import Twist
from cv_bridge import CvBridge, CvBridgeError

# Routines
from helpers import toMAT, img_processing, get_contour

class Position:

    def __init__(self):
        ''''''''''''''''''''''''''''''''''''''''''''''''''''
        Class constructor
        '''''''''''''''''''''''''''''''''''''''''''''''''''''

        #Size of the camera
        self.x = 640
        self.y = 480

        # Instances for objects
        self.velocity = Twist()
        self.bridge = CvBridge()
        self.tf_listener = tf.TransformListener()

        # Flags
        self.img_centered = False
        self.ar_positioned = False

        # Rate
        self.rate = rospy.Rate(10)

        # Velocity Publisher
        self.velocity_pub = rospy.Publisher('mobile_base/commands/velocity', Twist, queue_size = 10)


    def center_image(self, raw_image):
        ''''''''''''''''''''''''''''''''''''''''''''''''''''
        Centers the image in order to be in the center of
        the image frame.
        Args:image_raw: RGB raw image
        '''''''''''''''''''''''''''''''''''''''''''''''''''''

        # RGB raw image to bgr Mat format
        cv_image = toMAT(raw_image)

        # Image processing
        processed_img = img_processing(cv_image)

        # Get the contours
        contours, maxCnt_index = get_contour(processed_img)

        # Draw the contours
        cv2.drawContours(cv_image, contours, maxCnt_index, (0,255,0), 3)

        # Contours in order to center the image
        M = cv2.moments(contours[maxCnt_index])
        max_cnt_y = int(M['m01']/M['m00'])
        max_cnt_x = int(M['m10']/M['m00'])


        # Centering the image within the image frame
        if self.x - max_cnt_x > 340:
            self.velocity.angular.z = 0.2

        elif self.x - max_cnt_x < 280:
            self.velocity.angular.z = -0.2

        else:
            # Stops the rotation
            self.velocity.angular.z = 0
            self.img_centered = True

        # Velocity Publisher
        self.velocity_pub.publish(self.velocity)


    def ar_in_position(self):
            ''''''''''''''''''''''''''''''''''''''''''''''''''''
            Return if the AR Marker is in the image frame
            Returns: True or False
            '''''''''''''''''''''''''''''''''''''''''''''''''''''
            return self.ar_positioned

    def is_img_centered(self):
        """
            Returns the image centering process.
            Returns:
                bool: True or False (image processing done or not)
        """
        return self.img_centered

    def reset_ar_flag(self):
        ''''''''''''''''''''''''''''''''''''''''''''''''''''
        Resets the AR Marker flag
        Returns: True or False
        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        self.ar_positioned = False

    def reset_center_flag(self):
        ''''''''''''''''''''''''''''''''''''''''''''''''''''
        Resets the centering flag to be available for a new search
        Returns: True or False
        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        self.img_centered = False
