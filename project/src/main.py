from __future__ import division
from __future__ import print_function


'''ROS and OpenCV packages'''
import rospy
import numpy as np
import cv2
import time
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import actionlib
from actionlib_msgs.msg import *
from geometry_msgs.msg import Pose, Point, Quaternion, Twist
import yaml
import os
import math
import sys
import tf
from cv_bridge import CvBridge, CvBridgeError

# Ar-marker message data type
from sensor_msgs.msg import Image

from ar_track_alvar_msgs.msg import AlvarMarkers
from modules import Position, Recognition
class Main():
        #identifer
    def __init__(self):
        ''''''''''''''''''''''''''''''''''''''''''''''''''''
        Class construction
        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        # Counter
        self.tag_ids = rospy.get_param('~tag_ids', None)


        # AR Markers array
        self.idname= ["Nick Efford","Sam Wilson","Mhd Ammar AlSalka","Tom Ranner","Fifth Door","SSO","Natasha Shakhlevich","Jill Duggleby","Mark Walkley","Lab","Further Offices","Haiko Muller","Evangelos Pournaras","Amy Lowe","SSO Meeting Room","Another room","Entrance/Exit"]

        # Flags
        self.process = True

        # Marker Data
        self.img_raw = None
        self.ar_data = None

        # Instances for objects
        self.pst = Position()
        self.rcg = Recognition()

        # Marker & Image subscribers
        self.ar_tracker = rospy.Subscriber('ar_pose_marker', AlvarMarkers, self.set_ar_data)
        self.image_raw = rospy.Subscriber('camera/rgb/image_raw', Image, self.set_raw_image)
        # Marker & Image subscribers
        self.goal_sent = False

        # What to do if shutdown
        rospy.on_shutdown(self.shutdown)

    	# Tell the action client that we want to spin a thread by default
    	self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)
    	rospy.loginfo("Wait for the action server to come up")

    	# Allow up to 5 seconds for the action server to come up
    	self.move_base.wait_for_server(rospy.Duration(5))

        #initialise vision module
        self.bridge = CvBridge()
        self.sensitivity = 10

        #initialise velocitty
        self.velocity = rospy.Publisher('mobile_base/commands/velocity', Twist, queue_size = 10)
        self.rate = rospy.Rate(10)
        self.desired_velocity = Twist()

        # wait for a second before continuing
        rospy.sleep(1)

    def goto(self, pos, quat,selected,index):

        ''''''''''''''''''''''''''''''''''''''''''''''''''''
        Go to the selected office and carry out detection and recognition of AR Markers
        Args:image_raw: position,qaternion, selected office, index number
        '''''''''''''''''''''''''''''''''''''''''''''''''''''

        # Send the position of the goal office
        self.goal_sent = True
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = 'map'
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose = Pose(Point(pos['x'], pos['y'], 0.000),
                                     Quaternion(quat['r1'], quat['r2'], quat['r3'], quat['r4']))

	    # Start moving
        self.move_base.send_goal(goal)

	    # Allow TurtleBot up to 150 seconds to complete task
        success = self.move_base.wait_for_result(rospy.Duration(150))

        state = self.move_base.get_state()
        result = False

        if success and state == GoalStatus.SUCCEEDED:
            #Turtlebot reached the selected office
            result = True
            entrance=navigator.doc['type']['points'][16]
            position = {'x': entrance[0], 'y' :entrance[1]}

            if  not(position==pos):
              self.detect_rec(selected,index)

        else:
            self.move_base.cancel_goal()

        self.goal_sent = False
        return result

    def detect_rec(self,selected,index):
        ''''''''''''''''''''''''''''''''''''''''''''''''''''
        Detection and Recognition module
        Args: selected office, index number
        '''''''''''''''''''''''''''''''''''''''''''''''''''''

        # Get latest ar data
        rotationended=0
        data = self.get_ar_data()
        checked=1
        self.process=True
        ''''''''''''''''''''''''''''''''''''''''''''''''''''
        Go to the selected office and carry out detection and recognition of AR Markers
        Args:image_raw: position,qaternion, selected office, index number
        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        self.desired_velocity.angular.z = math.radians(self.findrotation(index))
        self.velocity.publish(self.desired_velocity)

        #allow 5 seconds for the rotation
        start = time.time()
        now=time.time()
        while (now-start)<5:
            now=time.time()
        self.rate.sleep()
        rotationended=1

        #rotate until the AR Marker found
        while not bool(data) and self.process and rotationended==1:
                    data=self.get_ar_data()


                    self.desired_velocity.linear.x = 0
                    self.desired_velocity.angular.z = math.radians(10)
                    self.velocity.publish(self.desired_velocity)
                    self.rate.sleep()

        # Run the Detection and Recognition Module
        if bool(data) and self.process :
            while not self.rcg.is_recognised():

                # Center the marker in the Camera Feed
                if self.pst.ar_in_position and not self.pst.img_centered:
                    rospy.loginfo("Image centering...")
                    self.pst.center_image(self.get_raw_image())


                # Recognition
                elif self.pst.ar_in_position and self.pst.img_centered:
                    rospy.loginfo("Recognition...")
                    # Mark the recognition flag as true
                    self.ar = self.get_ar_data()
                    self.rcg.recognised = True

                    # Check if the marker belongs to the correct member of the staff
                    self.foundname=self.idname[self.ar[0].id]
                    if (selected==self.foundname):
                        print("Door is correct. You are outside %s door." %self.foundname  )
                    else:
                        print("Door is incorrect. This door belong to %s" %self.foundname)

                else:
                    # Break the loop
                    print("Detection not working...")
                    break

            # Start new search
            self.process = False
            self.pst.reset_ar_flag()
            self.rcg.reset_rec_flag()
            self.pst.reset_center_flag()

    #Rotation needed for each office
    def findrotation(self,index):
        switcher={
            0:45,
            1:60,
            2:90,
            3:90,
            4:90,
            5:90,
            6:120,
            7:140,
            8:90,
            9:-120,
            10:280,
            11:270,
            12:280,
            13:270,
            14:290,
            15:280,
            16:320
        }
        return switcher.get(index,"Invalid door")


    def set_raw_image(self, data):
        ''''''''''''''''''''''''''''''''''''''''''''''''''''
        Sets the camera feed
        Args:image_raw: image data
        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        self.img_raw = data
        self.bridge = CvBridge()
        convimg = self.bridge.imgmsg_to_cv2(self.img_raw, "bgr8")

        cv2.imshow('Camera_Feed',convimg)
        cv2.waitKey(1)

    def set_ar_data(self, msg):
        ''''''''''''''''''''''''''''''''''''''''''''''''''''
        Sets the AR data
        Args:image_raw: AR Marker Data
        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        self.ar_data = msg

    def get_raw_image(self):
        ''''''''''''''''''''''''''''''''''''''''''''''''''''
        Return the latest image raw data
        Return:RGB image
        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        return self.img_raw


    def get_ar_data(self):
        ''''''''''''''''''''''''''''''''''''''''''''''''''''
        Return the latest ar marker data
        Return:coordinates of the ar markers
        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        return self.ar_data.markers


    def shutdown(self):
        if self.goal_sent:
            self.move_base.cancel_goal()
        rospy.loginfo("Stop")
        rospy.sleep(1)


if __name__ == '__main__':
    try:
        rospy.init_node('nav_test', anonymous=False)
        navigator = Main()

        #get into the points file to get the coordinates of each office
        with open('points.yaml','r') as f:
		    navigator.doc=yaml.load(f)

        #get into names file to get all the staff members name
        with open('names.yaml','r') as n:
            offices=yaml.load(n)

        begin=1
        while begin>0:
          #Show the menu
          print("Welcome to Long Room Navigation")
          print("Below is the list of offices")

          for n in offices['type']['names']:
              print(n)

          name=raw_input("Enter the number of the list above or a name:")
          listnames=[]
          counter=0
          for n in offices['type']['names']:
              counter=counter+1
              if name in n:
                  listnames.append(counter-1)
          while len(listnames)==0:
              counter=0
              name=raw_input("Wrong. Please enter the number of the list above or a name:")
              for n in offices['type']['names']:
                  counter=counter+1
                  if name in n:
                      listnames.append(counter-1)
          if len(listnames)==1:
              pos=listnames[0]
              currentname=offices['type']['names'][pos]
              #remove the digits
              result = ''.join([i for i in currentname if not i.isdigit()])
              selected=result[2:]
          else:

              print('According to your search we found: %d options. Please enter the number of the office you want to visit from the list below:'%(len(listnames)))
              countoptions=1

              for l in listnames:
                  currentname=offices['type']['names'][l]
                  #remove the digits
                  result = ''.join([i for i in currentname if not i.isdigit()])
                  print('%d. %s'%(countoptions,result[2:]))
                  countoptions=countoptions+1
              option=raw_input("Option: ")

              if option.isdigit():
                  option=int(option)

              while (option<=0  or option>countoptions or type(option)==str):
                  print("Wrong option please select a valid number")
                  option=raw_input("Option: ")

                  if option.isdigit():
                      option=int(option)

              option=int(option)-1
              pos=listnames[option]
              currentname=offices['type']['names'][pos]
              result = ''.join([i for i in currentname if not i.isdigit()])
              selected=result[2:]

          x= navigator.doc['type']['points'][pos]
    	  position = {'x': x[0], 'y' :x[1]}
    	  quaternion = {'r1' : 0.000, 'r2' : 0.000, 'r3' : 0.000, 'r4' : 1.000}
          #run the navigation
    	  rospy.loginfo("Go to (%s, %s) pose", position['x'], position['y'])
    	  success = navigator.goto(position, quaternion,selected,pos)

    	  if success:
              rospy.loginfo("Going back to the entrance door.")
              new_pos=navigator.doc['type']['points'][16]
              y= {'x': new_pos[0], 'y' :new_pos[1]}
              success=navigator.goto(y,quaternion,selected,pos)
          else:
         		rospy.loginfo("The base failed to reach the desired pose")

    except rospy.ROSInterruptException:
        rospy.loginfo("Ctrl-C found. Quitting")
